import { NextFunction, Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { HttpStatusCode } from '../application/enums/http-statuses.enum';

export const getAllGroups = (request: Request, response: Response) => {
  const groups = groupsService.getAllGroups();
  response.status(HttpStatusCode.OK).json(groups);
};

export const getGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const groups = groupsService.getGroupById(id);
  response.status(HttpStatusCode.OK).json(groups);
};

export const createGroup = (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = groupsService.createGroup(request.body);
  response.status(HttpStatusCode.CREATED).json(group);
};

export const updateGroupById = (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = groupsService.updateGroupById(id, request.body);
  response.status(HttpStatusCode.OK).json(student);
};

export const deleteGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.deleteGroupById(id);
  response.status(HttpStatusCode.OK).json(group);
};
