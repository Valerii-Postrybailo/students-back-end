import { getAllStudent } from '../students/students.model';
import { IGroup, IGroupWithStudents } from './types/group.interface';
import ObjectID from 'bson-objectid';
import { students as importedStudents } from '../students/students.model';

const groups: IGroup[] = [
  {
    id: ObjectID().toHexString(),
    name: 'G-1',
  },

  {
    id: ObjectID().toHexString(),
    name: 'G-2',
  },
];

export const getAllGroups = (): IGroupWithStudents[] => {
  const students = importedStudents;

  const updatedGroups = groups.map((group) => ({
    ...group,
    students: students.filter((student) => student.groupId === group.id),
  }));
  updatedGroups.forEach((group) => {
    const groupId = group.id;
    const groupStudents = students.filter(
      (student) => student.groupId === groupId,
    );
    group.students = groupStudents;
  });

  return updatedGroups;
};

export const getGroupById = (
  groupId: string,
): IGroupWithStudents | undefined => {
  const students = getAllStudent();

  const foundGroup = groups.find(({ id }) => id === groupId);
  if (!foundGroup) {
    return undefined;
  }

  const groupStudents = students.filter(
    (student) => student.groupId === groupId,
  );

  const groupWithStudents: IGroupWithStudents = {
    ...foundGroup,
    students: groupStudents,
  };

  return groupWithStudents;
};

export const createGroup = (createGroupSchema: Omit<IGroup, 'id'>): IGroup => {
  const newGroup = {
    ...createGroupSchema,
    id: ObjectID().toHexString(),
  };

  groups.push(newGroup);
  return newGroup;
};

export const updateGroupById = (
  groupId: string,
  updateGroupSchema: Partial<IGroup>,
): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const student = groups[groupIndex];

  if (!student) {
    return;
  }

  const updatedStudent = {
    ...student,
    ...updateGroupSchema,
  };

  groups.splice(groupIndex, 1, updatedStudent);

  return updatedStudent;
};

export const deleteGroupById = (groupId: string): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];

  if (!group) {
    return;
  }

  groups.splice(groupIndex, 1);
  return group;
};
