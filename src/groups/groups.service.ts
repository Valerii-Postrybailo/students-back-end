import { HttpStatusCode } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as groupsModel from './groups.model';
import { IGroup } from './types/group.interface';

export const getAllGroups = () => {
  return groupsModel.getAllGroups();
};

export const getGroupById = (id: string) => {
  const group = groupsModel.getGroupById(id);
  if (!group) {
    throw new HttpException(HttpStatusCode.NOT_FOUND, 'Group not found');
  }
  return group;
};

export const createGroup = (groupCreateSchema: Omit<IGroup, 'id'>) => {
  return groupsModel.createGroup(groupCreateSchema);
};

export const updateGroupById = (
  id: string,
  groupUpdateSchema: Partial<IGroup>,
) => {
  const updatedGroup = groupsModel.updateGroupById(id, groupUpdateSchema);

  if (!updatedGroup) {
    throw new HttpException(HttpStatusCode.NOT_FOUND, 'Group not found');
  }

  return updatedGroup;
};

export const deleteGroupById = (id: string) => {
  const deleteGroup = groupsModel.deleteGroupById(id);

  if (!deleteGroup) {
    throw new HttpException(HttpStatusCode.NOT_FOUND, 'Group not found');
  }

  return deleteGroup;
};
