import { Router } from 'express';
import controllerWrapper from '../application/utilities/controller-wrapper';
import * as groupsController from './groups.controller';
import { idParamSchema } from '../application/schemas/id-param.schema';
import validator from '../application/middlewares/validation.middleware';
import { groupCreateSchema, groupUpdateSchema } from './groups.schema';

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroups));

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupById),
);

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.deleteGroupById),
);

export default router;
