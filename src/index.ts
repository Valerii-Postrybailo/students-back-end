import app from './application/app';
import 'dotenv/config';

const port = process.env.SERVER_PORT;

const startServer = async () => {
  app.listen(port, () => {
    console.log(`Server started at port ${port}`);
  });
};

startServer();
