import { Router } from 'express';
import uploadMiddleware from '../application/middlewares/upload.middleware';
import validator from '../application/middlewares/validation.middleware';
import { idParamSchema } from '../application/schemas/id-param.schema';
import controllerWrapper from '../application/utilities/controller-wrapper';
import * as studentsController from './students.controller';
import {
  studentCreateSchema,
  studentGroupUpdateSchema,
  studentUpdateSchema,
} from './student.schema';
import { handleStudentNotFound } from '../application/middlewares/bad-id.middleware';

const router = Router();

router.get('/', controllerWrapper(studentsController.getAllStudents));

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentById),
);

router.get(
  '/:id/image',
  validator.params(idParamSchema),
  handleStudentNotFound,
  controllerWrapper(studentsController.getImageById),
);

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  handleStudentNotFound,
  controllerWrapper(studentsController.updateStudentById),
);

router.patch(
  '/:id/group',
  validator.params(idParamSchema),
  validator.body(studentGroupUpdateSchema),
  handleStudentNotFound,
  controllerWrapper(studentsController.addGroup),
);

router.patch(
  '/:id/image',
  validator.params(idParamSchema),
  uploadMiddleware.single('file'),
  handleStudentNotFound,
  controllerWrapper(studentsController.addImage),
);

router.delete(
  '/:id',
  validator.params(idParamSchema),
  handleStudentNotFound,
  controllerWrapper(studentsController.deleteStudentById),
);

export default router;
