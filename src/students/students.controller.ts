import { NextFunction, Request, Response } from 'express';
import * as studentService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { HttpStatusCode } from '../application/enums/http-statuses.enum';

export const getAllStudents = (request: Request, response: Response) => {
  const students = studentService.getAllStudents();
  response.status(HttpStatusCode.OK).json(students);
};

export const getStudentById = (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const students = studentService.getStudentById(id);
  response.status(HttpStatusCode.OK).json(students);
};

export const getImageById = (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const student = studentService.getStudentById(id);
  const { imagePath } = student;
  response.status(HttpStatusCode.OK).json({ imagePath });
};

export const createStudent = (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = studentService.createStudent(request.body);
  response.status(HttpStatusCode.CREATED).json(student);
};

export const updateStudentById = (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentService.updateStudentById(id, request.body);
  response.status(HttpStatusCode.OK).json(student);
};

export const addGroup = (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const groupId = request.body.groupId;
  if (groupId !== undefined) {
    const student = studentService.addGroup(id, groupId);
    response.status(HttpStatusCode.OK).json(student);
  } else {
    response.status(HttpStatusCode.BAD_REQUEST).json('Missing required fields');
  }
};

export const addImage = (
  request: Request<{ id: string; file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};

  studentService
    .addImage(id, path)
    .then((student) => {
      response.status(HttpStatusCode.OK).json(student);
    })
    .catch((error) => {
      console.error(error);
      response
        .status(HttpStatusCode.INTERNAL_SERVER_ERROR)
        .json({ error: 'An error occurred' });
    });
};

export const deleteStudentById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentService.deleteStudentById(id);
  response.status(HttpStatusCode.OK).json(student);
};
