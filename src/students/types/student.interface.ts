import { IGroup } from '../../groups/types/group.interface';

export interface IStudent {
  id: string;
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath: string | null;
  groupId: string | null;
}

export interface IStudentWithGroups extends IStudent {
  groupName: string | null;
}
