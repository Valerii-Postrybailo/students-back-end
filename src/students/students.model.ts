import { getAllGroups } from '../groups/groups.model';
import { IStudent, IStudentWithGroups } from './types/student.interface';
import ObjectID from 'bson-objectid';

export const students: IStudent[] = [
  {
    id: ObjectID().toHexString(),
    name: 'Bill',
    surname: 'Harington',
    age: 46,
    email: 'tomh@gmail.com',
    imagePath: null,
    groupId: null,
  },

  {
    id: ObjectID().toHexString(),
    name: 'Boris',
    surname: 'Animal',
    age: 33,
    email: 'borisa@gmail.com',
    imagePath: 'path',
    groupId: null,
  },
];

const groups = getAllGroups();

export const getAllStudent = (): IStudent[] => {
  const updatedStudent = students.map((student) => {
    const studentGroups = groups
      .filter((group) => student.groupId === group.id)
      .map((group) => group.name)
      .join(', ');

    const groupsValue = studentGroups || null;

    return {
      ...student,
      groupName: groupsValue,
    };
  });

  return updatedStudent;
};

export const getStudentById = (
  studentId: string,
): IStudentWithGroups | undefined => {
  const student = students.find(({ id }) => id === studentId);

  if (student) {
    const groupName = groups
      .filter((group) => student.groupId === group.id)
      .map((group) => group.name)
      .join(', ');

    const groupsValue = groupName || null;

    return {
      ...student,
      groupName: groupsValue,
    };
  }

  return undefined;
};

export const createStudent = (
  createStudentSchema: Omit<IStudent, 'id'>,
): IStudent => {
  const newStudent = {
    ...createStudentSchema,
    id: ObjectID().toHexString(),
  };

  students.push(newStudent);
  return newStudent;
};

export const updateStudentById = (
  studentId: string,
  updateStudentSchema: Partial<IStudent>,
): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    return;
  }

  const updatedStudent = {
    ...student,
    ...updateStudentSchema,
  };

  students.splice(studentIndex, 1, updatedStudent);

  return updatedStudent;
};

export const updateStudentGroupById = (
  studentId: string,
  updateStudentSchema: string | null,
): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];
  student.groupId = updateStudentSchema;
  if (!student) {
    return;
  }

  const updatedStudent = {
    ...student,
  };

  students.splice(studentIndex, 1, updatedStudent);

  return updatedStudent;
};

export const deleteStudentById = (studentId: string): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    return;
  }

  students.splice(studentIndex, 1);
  return student;
};
