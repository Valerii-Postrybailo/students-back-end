import ObjectID from 'bson-objectid';
import path from 'path';
import fs from 'fs/promises';
import HttpException from '../application/exceptions/http-exception';
import { HttpStatusCode } from '../application/enums/http-statuses.enum';
import * as studentsModel from './students.model';
import { IStudent } from './types/student.interface';

export const getAllStudents = () => {
  return studentsModel.getAllStudent();
};

export const getStudentById = (id: string) => {
  const student = studentsModel.getStudentById(id);
  if (!student) {
    throw new HttpException(HttpStatusCode.NOT_FOUND, 'Student not found');
  }
  return student;
};

export const createStudent = (studentCreateSchema: Omit<IStudent, 'id'>) => {
  return studentsModel.createStudent(studentCreateSchema);
};

export const updateStudentById = (
  id: string,
  studentUpdateSchema: Partial<IStudent>,
) => {
  const updatedStudent = studentsModel.updateStudentById(
    id,
    studentUpdateSchema,
  );

  return updatedStudent;
};

export const addGroup = (id: string, groupId: string | null) => {
  const updatedStudent = studentsModel.updateStudentGroupById(id, groupId);
  return updatedStudent;
};

export const addImage = async (id: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatusCode.BAD_REQUEST, 'File is not provided');
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtention = path.extname(filePath);
    const imageName = imageId + imageExtention;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(
      __dirname,
      '../',
      'public',
      studentsDirectoryName,
    );
    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updateStudentById(id, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};

export const deleteStudentById = (id: string) => {
  const deleteStudent = studentsModel.deleteStudentById(id);
  return deleteStudent;
};
