import Joi from 'joi';
import { IStudent } from './types/student.interface';

export const studentCreateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().required(),
  surname: Joi.string().required(),
  email: Joi.string().required(),
  age: Joi.number().required(),
  imagePath: Joi.string().allow(null).required(),
  groupId: Joi.string().allow(null).required(),
});

export const studentUpdateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().optional(),
  surname: Joi.string().optional(),
  email: Joi.string().optional(),
  age: Joi.number().optional(),
  imagePath: Joi.string().allow(null).optional(),
  groupId: Joi.string().allow(null).optional(),
});

export const studentGroupUpdateSchema = Joi.object({
  groupId: Joi.string().required(),
});
