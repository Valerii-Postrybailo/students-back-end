import express from 'express';
import cors from 'cors';
import path from 'path';

import studentsRouter from '../students/students.router';
import bodyParser from 'body-parser';
import exceptionsFilter from './middlewares/exceptions.filter';
import groupsRouter from '../groups/groups.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());

const staticFilesPath = path.join(__dirname, '../', 'public');
app.use('/api/v1/public', express.static(staticFilesPath));

app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);


app.use(exceptionsFilter);

export default app;
