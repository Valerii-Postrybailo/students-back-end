import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/http-exception';
import { HttpStatusCode } from '../enums/http-statuses.enum';
import * as studentsModel from '../../students/students.model';

interface CustomRequest extends Request {
  student?: any;
}

export const handleStudentNotFound = (
  req: CustomRequest,
  res: Response,
  next: NextFunction,
) => {
  try {
    const student = studentsModel.getStudentById(req.params.id);
    if (!student) {
      throw new HttpException(HttpStatusCode.NOT_FOUND, 'Student not found');
    }
    req.student = student;
    next();
  } catch (error) {
    next(error);
  }
};
