# students-back-end

"@types/node": "^20.3.1",
"@typescript-eslint/eslint-plugin": "^5.59.11",
"@typescript-eslint/parser": "^5.59.11",
"eslint": "^8.42.0",
"eslint-config-prettier": "^8.8.0",
"eslint-plugin-prettier": "^4.2.1",
"express": "^4.18.2",
"nodemon": "^2.0.22",
"prettier": "^2.8.8",
"ts-node": "^10.9.1",
"typescript": "^5.1.3"
